<?php
$servername = "localhost";
$username = "root";
$password = "";
$database = "pixel8";

$conn = mysqli_connect($servername, $username, $password, $database);

if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

// Create
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST["create"])) {
    $first_name = $_POST["first_name"];
    $last_name = $_POST["last_name"];
    $address = $_POST["address"];
    $birthday = $_POST["birthday"];

    $sql = "INSERT INTO employee (first_name, last_name, address, birthday) VALUES ('$first_name', '$last_name', '$address', '$birthday')";

    if (mysqli_query($conn, $sql)) {
        echo "<h1>Record created successfully please reload the page</h1>";
    } else {
        echo "Error creating record: " . mysqli_error($conn);
    }
}

// Read
$sql = "SELECT * FROM employee";
$result = mysqli_query($conn, $sql);

if ($result) {
    echo "<h2>Employee List</h2>";
    echo "<table border='1'>
            <tr>
                <th>ID</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Address</th>
                <th>Birthday</th>
            </tr>";
    
    while ($row = mysqli_fetch_assoc($result)) {
        echo "<tr>
                <td>{$row['id']}</td>
                <td>{$row['first_name']}</td>
                <td>{$row['last_name']}</td>
                <td>{$row['address']}</td>
                <td>{$row['birthday']}</td>
            </tr>";
    }
    
    echo "</table>";
} else {
    echo "Error reading records: " . mysqli_error($conn);
}

// Update
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST["update"])) {
    $id_to_update = $_POST["id_to_update"];
    $new_first_name = $_POST["new_first_name"];
    $new_last_name = $_POST["new_last_name"];
    $new_birthday = $_POST["new_birthday"];
    $new_address = $_POST["new_address"];

    $sql = "UPDATE employee SET first_name = '$new_first_name', last_name = '$new_last_name', birthday = '$new_birthday', address = '$new_address' WHERE id = $id_to_update";

    if (mysqli_query($conn, $sql)) {
        echo "<h1>Record updated successfully please reload the page</h1>";
    } else {
        echo "Error updating record: " . mysqli_error($conn);
    }
}

// Delete
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST["delete"])) {
    $id_to_delete = $_POST["id_to_delete"];

    $sql = "DELETE FROM employee WHERE id = $id_to_delete";

    if (mysqli_query($conn, $sql)) {
        echo "<h1>Record deleted successfully please reload the page</h1>";
    } else {
        echo "Error deleting record: " . mysqli_error($conn);
    }
}

mysqli_close($conn);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CRUD Example</title>
</head>
<body>
    <h2>Create Record</h2>
    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
        First Name: <input type="text" name="first_name" required><br>
        Last Name: <input type="text" name="last_name" required><br>
        Address: <input type="text" name="address" required><br>
        Birthday: <input type="date" name="birthday" required><br>
        <input type="submit" name="create" value="Create Record" onClick="window.location.reload();">
    </form>

    <h2>Update Record</h2>
    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
        ID to Update: <input type="number" name="id_to_update" required><br>
        First Name: <input type="text" name="new_first_name" required><br>
        Last Name: <input type="text" name="new_last_name" required><br>
        Birthday: <input type="date" name="new_birthday" required><br>
        Address: <input type="text" name="new_address" required><br>
        <input type="submit" name="update" value="Update Record" onClick="window.location.reload();">
    </form>

    <h2>Delete Record</h2>
    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
        ID to Delete: <input type="number" name="id_to_delete" required><br>
        <input type="submit" name="delete" value="Delete Record">
    </form>
</body>
</html>