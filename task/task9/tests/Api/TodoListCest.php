<?php

namespace Tests\Api;

use Tests\Support\ApiTester;

class TodoListCest
{
    public function _before(ApiTester $I)
    {
    }
    public function _after(ApiTester $I)
    {
    }

    public function iShouldInsertData(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPost('/todo-application/task/task9/API.php', [
            'task_title' => 'Create Backend API',
            'task_name' => 'API'
            // Add other fields as needed
        ]);

        // Debugging statements
        $response = $I->grabResponse();
        codecept_debug($response);

        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['success' => true, 'message' => 'Data inserted successfully']);
    }

    public function iShouldDeleteData(ApiTester $I)
    {
        // Assuming you have the task ID from the previous insert test
        $taskIdToDelete = 1; // Replace with the actual task ID

        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendDelete("/todo-application/task/task9/API.php/{$taskIdToDelete}", ['id' => $taskIdToDelete]);

        // Debugging statements
        $response = $I->grabResponse();
        codecept_debug($response);

        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['success' => true, 'message' => 'Data deleted successfully']);
    }

    public function iShouldUpdateData(ApiTester $I)
    {
        // Assuming you have the task ID from the previous insert test
        $taskIdToUpdate = 1; // Replace with the actual task ID

        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPut("/todo-application/task/task9/API.php/{$taskIdToUpdate}", [
            'id' => $taskIdToUpdate,
            'task_title' => 'Updated Task Title',
            'task_name' => 'Updated Task Name'
            // Add other fields to update as needed
        ]);

        // Debugging statements
        $response = $I->grabResponse();
        codecept_debug($response);

        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['success' => true, 'message' => 'Data updated successfully']);
    }

    public function iShouldFetchData(ApiTester $I)
    {

        $validPayload = ['status' => 'completed'];

        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendGet("/todo-application/task/task9/API.php", $validPayload);

        // Debugging statements
        $response = $I->grabResponse();
        codecept_debug($response);

        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['success' => true, 'message' => 'Data fetched successfully']);
        // Add additional assertions based on the expected data in the response
    }

    public function iCannotInsertDataWithInvalidPayload(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPost('/todo-application/task/task9/API.php', [
            // Add other fields as needed
        ]);

        // Debugging statements
        $response = $I->grabResponse();
        codecept_debug($response);

        $I->seeResponseCodeIs(400);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['success' => false, 'message' => 'Invalid payload. Non-empty array expected.']);
    }

    public function iCannotUpdateDataWithInvalidId(ApiTester $I)
    {
        // Assuming you have the task ID from the previous insert test
        $taskIdToUpdate = 1; // Replace with the actual task ID

        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPut("/todo-application/task/task9/API.php/", [
            'task_title' => 'Updated Task Title',
            'task_name' => 'Updated Task Name'
            // Add other fields to update as needed
        ]);

        // Debugging statements
        $response = $I->grabResponse();
        codecept_debug($response);

        $I->seeResponseCodeIs(400);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['success' => false, 'message' => 'Invalid ID. ID cannot be null or empty.']);
    }
    public function iCannotUpdateDataWithInvalidPayload(ApiTester $I)
    {
        // Assuming you have the task ID from the previous insert test
        $taskIdToUpdate = 1; // Replace with the actual task ID

        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPut("/todo-application/task/task9/API.php/{$taskIdToUpdate}", [
            // Add other fields to update as needed
        ]);

        // Debugging statements
        $response = $I->grabResponse();
        codecept_debug($response);

        $I->seeResponseCodeIs(400);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['success' => false, 'message' => 'Invalid payload. Payload cannot be empty.']);
    }
    public function iCannotUpdateDataWithMismatchedId(ApiTester $I)
    {
        // Assuming you have the task ID from the previous insert test
        $taskIdToUpdate = 1; // Replace with the actual task ID

        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPut("/todo-application/task/task9/API.php/{$taskIdToUpdate}", [
            'id' => (!$taskIdToUpdate),
            'task_title' => 'Updated Task Title',
            'task_name' => 'Updated Task Name'
            // Add other fields to update as needed
        ]);

        // Debugging statements
        $response = $I->grabResponse();
        codecept_debug($response);

        $I->seeResponseCodeIs(404);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['success' => false, 'message' => 'Mismatched IDs. The provided ID does not match the payload ID.']);
    }
    public function iCannotDeleteDataWithInvalidId(ApiTester $I)
    {
        // Assuming you have the task ID from the previous insert test
        $taskIdToDelete = 1; // Replace with the actual task ID

        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPut("/todo-application/task/task9/API.php/", [
            // Add other fields to update as needed
        ]);

        // Debugging statements
        $response = $I->grabResponse();
        codecept_debug($response);

        $I->seeResponseCodeIs(400);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['success' => false, 'message' => 'Invalid ID. ID cannot be null or empty.']);
    }
    public function iCannotDeleteDataWithMismatchedId(ApiTester $I)
    {
        // Assuming you have the task ID from the previous insert test
        $taskIdToDelete = 1; // Replace with the actual task ID

        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPut("/todo-application/task/task9/API.php/{$taskIdToDelete}", [
            'id' => (!$taskIdToDelete),
            'task_title' => 'Deleted Task Title',
            'task_name' => 'Deleted Task Name'
            // Add other fields to update as needed
        ]);

        // Debugging statements
        $response = $I->grabResponse();
        codecept_debug($response);

        $I->seeResponseCodeIs(404);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['success' => false, 'message' => 'Mismatched IDs. The provided ID does not match the payload ID.']);
    }

}

?>