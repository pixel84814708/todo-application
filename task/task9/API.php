<?php

/**
   * Tells the browser to allow code from any origin to access
   */
  header("Access-Control-Allow-Origin: *");

  /**
   * Tells browsers whether to expose the response to the frontend JavaScript code
   * when the request's credentials mode (Request.credentials) is include
   */
  header("Access-Control-Allow-Credentials: true");
 
  /**
   * Specifies one or more methods allowed when accessing a resource in response to a preflight request
   */
  header("Access-Control-Allow-Methods: POST, GET, PUT, DELETE");
 
  /**
   * Used in response to a preflight request which includes the Access-Control-Request-Headers to
   * indicate which HTTP headers can be used during the actual request
   */
  header("Access-Control-Allow-Headers: Content-Type");
  // header("Content-Type: application/json");

  require_once('MysqliDb.php');
  class API {
    public function __construct()
    {
        $this->db = new MysqliDB('localhost', 'root', '', 'employee');
    }
    private function getResponse($success, $message, $data = null) {
      $response = array(
          'success' => $success,
          'message' => $message,
          'data' => $data
      );
      echo json_encode($response);
    }
    /**
       * HTTP GET Request
       *
       * @param $payload
       */
      public function httpGet($payload)
      {
        //   if (!is_array($payload)) {
        //       // Return JSON response for invalid payload
        //       return $this->getResponse(false, "Invalid payload. Array expected.");
        //   }
      
        //   $this->db->where($payload);
          $result = $this->db->get('tbl_to_do_list');
      
          if ($result !== null) {
              // Return success response as JSON using getResponse
              return $this->getResponse(true, "Data fetched successfully", $result);
          } else {
              // Return failure response as JSON using getResponse
              return $this->getResponse(false, "Failed Fetch Request");
          }
      }
    /**
       * HTTP POST Request
       *
       * @param $payload
       */
    public function httpPost($payload){
      if (!is_array($payload) || empty($payload)) {
        http_response_code(400);
        return $this->getResponse(false, "Invalid payload. Non-empty array expected.");
      }
      $result = $this->db->insert('tbl_to_do_list', $payload);
      if ($result) {
        return $this->getResponse(true, "Data inserted successfully", $payload);
      } else {
        return $this->getResponse(false, "Failed to Insert Data");
      }
    }
    /**
    * HTTP PUT Request
    *  
    * @param $id
    * @param $payload
    */
    public function httpPut($id, $payload) {
      if (is_null($id) || empty($id)) {
        http_response_code(400);
        return $this->getResponse(false, "Invalid ID. ID cannot be null or empty.");
      }
      if (empty($payload)) {
        http_response_code(400);
        return $this->getResponse(false, "Invalid payload. Payload cannot be empty.");
      }
      if ($id != $payload['id']) {
        http_response_code(404);
        return $this->getResponse(false, "Mismatched IDs. The provided ID does not match the payload ID.");
      }

      // Check if the record with the specified ID exists before updating
      $existingRecord = $this->db->get('tbl_to_do_list', $id);

      if (!$existingRecord) {
        return $this->getResponse(false, "Record with ID $id not found.");
      }

      // Update the record
      $result = $this->db->where('id', $id)->update('tbl_to_do_list', $payload);

      if ($result) {
        return $this->getResponse(true, "Data updated successfully", $payload);
      } else {
        return $this->getResponse(false, "Failed to Update Data");
      }
    }
     /**
       * HTTP DELETE Request
       *
       * @param $id
       * @param $payload
       */
    public function httpDelete($id, $payload){
      if (empty($id)) {
        http_response_code(400);
        return $this->getResponse(false, "Invalid ID. ID cannot be null or empty.");
      }
      if (!isset($payload['id']) || $payload['id'] != $id) {
        http_response_code(404);
        return $this->getResponse(false, "ID in payload does not match the provided ID.");
      }
      if (!is_array($payload)) {
        $this->db->where('id', $id);
      } else {
        $this->db->where('id', $payload, 'IN');
      }
      $result = $this->db->delete('tbl_to_do_list');
      if ($result) {
        return $this->getResponse(true, "Data deleted successfully", $payload);
      } else {
        return $this->getResponse(false, "Failed to Delete Data");
      }
    }
  }

  //Identifier if what type of request
  $request_method = $_SERVER['REQUEST_METHOD'];
   // For GET,POST,PUT & DELETE Request
  if ($request_method === 'GET') {
    $received_data = $_GET;
  } else {
    //check if method is PUT or DELETE, and get the ids on URL
    if ($request_method === 'PUT' || $request_method === 'DELETE') {
      $request_uri = $_SERVER['REQUEST_URI'];
      $ids = null;
      $exploded_request_uri = array_values(explode("/", $request_uri));
      $last_index = count($exploded_request_uri) - 1;
      $ids = $exploded_request_uri[$last_index];
    }
  }
  //payload data
  $received_data = json_decode(file_get_contents('php://input'), true);
  $api = new API;
  //Checking if what type of request and designating to specific functions
  switch ($request_method) {
    case 'GET':
      $api->httpGet($received_data);
      break;
    case 'POST':
      $api->httpPost($received_data);
      break;
    case 'PUT':
      $api->httpPut($ids, $received_data);
      break;
    case 'DELETE':
      $api->httpDelete($ids, $received_data);
      break;
  }
?>