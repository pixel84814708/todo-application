<?php
class API {
    public function __construct($age, $email, $bday){
        $this->age = $age;
        $this->email = $email;
        $this->bday = $bday;
    }
    public function print_fullname($name){
        echo "Full Name: " . $name . "<br>";
    }
    public function print_hobbies($hobbies){
        echo "Hobbies: <br>";
        foreach ($hobbies as $hobby){
            echo "&nbsp&nbsp&nbsp&nbsp" . $hobby . "<br>";
        }
    }
    public function info(){
        echo "Age: " . $this->age . "<br>";
        echo "Email: " . $this->email . "<br>";
        echo "Birthday: " . $this->bday . "<br>";
    }
}
$myInfo = new API($age = 22, 
                    $email = "balagtasmichaeljoshua@gmail.com", 
                    $bday = "September 26, 2001");
$name = "Michael Joshua Balagtas";
$myInfo->print_fullname($name);
$hobbies = array("Playing Basketball", "Reading Books", "Listening Music");
$myInfo->print_hobbies($hobbies);
$myInfo->info();
?>