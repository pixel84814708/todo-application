<?php

require_once 'vendor/autoload.php';
require_once 'vendor/thingengineer/mysqli-database-class/MysqliDb.php';

$db = new MysqliDb (Array (
    'host' => 'localhost',
    'username' => 'root', 
    'password' => '',
    'db' => 'pixel8',
    'port' => 3306,
    'charset' => 'utf8'
));

$db->setPrefix(''); // or $db->setPrefix('my_');
$db->autoReconnect = false;

if (!$db->ping()) {
    die("Connection failed: " . $db->getLastError());
}

// Create
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST["create"])) {
    $first_name = $_POST["first_name"];
    $last_name = $_POST["last_name"];
    $address = $_POST["address"];
    $birthday = $_POST["birthday"];

    $data = Array (
        'first_name' => $first_name,
        'last_name' => $last_name,
        'address' => $address,
        'birthday' => $birthday
    );

    $id = $db->insert ('employee', $data);

    if ($id) {
        echo "<h1>Record created successfully please reload the page</h1>";
    } else {
        echo "Error creating record: " . $db->getLastError();
    }
}

// Read
$employees = $db->get ('employee');

if ($employees) {
    echo "<h2>Employee List</h2>";
    echo "<table border='1'>
            <tr>
                <th>ID</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Address</th>
                <th>Birthday</th>
            </tr>";

    foreach ($employees as $employee) {
        echo "<tr>
                <td>{$employee['id']}</td>
                <td>{$employee['first_name']}</td>
                <td>{$employee['last_name']}</td>
                <td>{$employee['address']}</td>
                <td>{$employee['birthday']}</td>
            </tr>";
    }

    echo "</table>";
} else {
    echo "Error reading records: " . $db->getLastError();
}

// Update
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST["update"])) {
    $id_to_update = $_POST["id_to_update"];
    $new_first_name = $_POST["new_first_name"];
    $new_last_name = $_POST["new_last_name"];
    $new_birthday = $_POST["new_birthday"];
    $new_address = $_POST["new_address"];

    $data = Array (
        'first_name' => $new_first_name,
        'last_name' => $new_last_name,
        'birthday' => $new_birthday,
        'address' => $new_address
    );

    $db->where ('id', $id_to_update);
    if ($db->update ('employee', $data)) {
        echo "<h1>Record updated successfully please reload the page</h1>";
    } else {
        echo "Error updating record: " . $db->getLastError();
    }
}

// Delete
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST["delete"])) {
    $id_to_delete = $_POST["id_to_delete"];

    $db->where ('id', $id_to_delete);
    if ($db->delete ('employee')) {
        echo "<h1>Record deleted successfully please reload the page</h1>";
    } else {
        echo "Error deleting record: " . $db->getLastError();
    }
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CRUD Example</title>
</head>
<body>
    <h2>Create Record</h2>
    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
        First Name: <input type="text" name="first_name" required><br>
        Last Name: <input type="text" name="last_name" required><br>
        Address: <input type="text" name="address" required><br>
        Birthday: <input type="date" name="birthday" required><br>
        <input type="submit" name="create" value="Create Record" onClick="window.location.reload();">
    </form>

    <h2>Update Record</h2>
    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
        ID to Update: <input type="number" name="id_to_update" required><br>
        First Name: <input type="text" name="new_first_name" required><br>
        Last Name: <input type="text" name="new_last_name" required><br>
        Birthday: <input type="date" name="new_birthday" required><br>
        Address: <input type="text" name="new_address" required><br>
        <input type="submit" name="update" value="Update Record" onClick="window.location.reload();">
    </form>

    <h2>Delete Record</h2>
    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
        ID to Delete: <input type="number" name="id_to_delete" required><br>
        <input type="submit" name="delete" value="Delete Record">
    </form>
</body>
</html>